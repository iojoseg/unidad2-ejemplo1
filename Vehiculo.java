public class Vehiculo{

  // atributos
  private String marca;
  private String modelo;
  private String color;
  private String matricula;
  
  // constructor sin argumentos
  public Vehiculo(){
  marca = "Chevrolet";
  modelo = "2020";
  color = "Azul";
  matricula = "YAG-2021H";
  }
  
  // constructor con argumentos
  public Vehiculo(String marca, String modelo, String color, String matricula){
  this.marca = marca;
  this.modelo = modelo;
  this.color = color;
  this.matricula = matricula;
  }
  
  // un metodo para mostrar informacion
  public void showAtributos(){
    System.out.println("Marca: " + marca);
    System.out.println("Modelo: " + modelo);
    System.out.println("Color: " + color);
    System.out.println("Matricula: " + matricula);
  }
  
  // metodos setters y getters 
  // metodo set  me sirve par asignar
  // metodo get me sirve para obtener
  
  public String getMarca(){
  return marca;
  }
  
  public void setMarca(String marca){
  this.marca = marca;
  }
  
}